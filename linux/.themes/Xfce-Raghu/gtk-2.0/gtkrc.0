# Theme: Xfce-Raghu
# Author: Raghu Rao
# Created: 10/25/2009
# Released under the GPL
# A simple, minimal theme that helps you focus on your work

style "default"
{
    GtkWidget::focus-line-pattern                = "\0"
    GtkWidget::focus-line-width                  = 1
    GtkWidget::focus_padding                     = 4
    GtkWidget::interior_focus                    = 5
    GtkWidget::internal_padding                  = 4
    GtkButton::default_border                    = {0, 0, 0, 0}
    GtkButton::default_outside_border            = {0, 0, 0, 0}
    GtkButton::child_displacement_x              = 0
    GtkButton::child_displacement_y              = 1
    GtkButton::default_spacing                   = 5
    GtkButton::focus-padding                     = 1
    GtkMenuItem::selected_shadow_type            = in
    GtkMenuItem::internal_padding                = 5
    GtkPaned::handle_full_size                   = 1
    GtkPaned::handle_size                        = 7
    GtkRange::slider_width                       = 14
    GtkRange::stepper_size                       = 14
    GtkRange::stepper_spacing                    = 0
    GtkRange::trough_border                      = 1
    GtkScrollbar::has_backward_stepper           = 1
    GtkScrollbar::has_secondary_backward_stepper = 0
    GtkScrollbar::has_secondary_forward_stepper  = 0
    GtkScrollbar::min_slider_length              = 20

    GtkCheckButton::indicator_size               = 12
    GtkRadioButton::indicator_size               = 13
    ExoIconBar::focus_padding                    = 2

    xthickness = 2
    ythickness = 2

    fg[NORMAL]       = "#000000"
    bg[NORMAL]       = "#dfdfdf"
    base[NORMAL]     = "#ffffff"
    text[NORMAL]     = "#000000"

    fg[ACTIVE]       = "#000000"
    bg[ACTIVE]       = "#c0c0c0"
    base[ACTIVE]     = "#dfdfdf"
    text[ACTIVE]     = "#000000"

    fg[PRELIGHT]     = "#000000"
    bg[PRELIGHT]     = "#dfdfdf"
    base[PRELIGHT]   = "#ffffff"
    text[PRELIGHT]   = "#000000"

    fg[SELECTED]     = "#ffffff"
    bg[SELECTED]     = "#326dd4"
    base[SELECTED]   = "#326dd4"
    text[SELECTED]   = "#ffffff"

    fg[INSENSITIVE]  = "#606060" 
    bg[INSENSITIVE]  = "#dfdfdf"
    base[INSENSITIVE]= "#dfdfdf"
    text[INSENSITIVE]= "#c0c0c0"

    engine "xfce"
    {
        #grip_style = none
        smooth_edge = true
        boxfill
        {
            fill_style = gradient
            orientation = vertical
            shade_start = 1.00
            shade_end = 0.97
        }
    }
}

style "progress" = "default"
{
    xthickness = 1
    ythickness = 1

    bg[ACTIVE]        = "#c0c0c0"
    bg[NORMAL]        = "#dfdfdf"
    bg[PRELIGHT]      = "#326dd4"

    fg[PRELIGHT]      = "#ffffff"

    engine "xfce"
    {
        grip_style = none
        smooth_edge = false
    }
}

style "titlebar" = "default"
{
    #fg[INSENSITIVE]   = "#aaaaaa"
    #bg[INSENSITIVE]   = "#aaaaaa"
    fg[SELECTED]      = "#ffffff"
    bg[SELECTED]      = "#326dd4"
}

style "scrollbar" = "default"
{
    engine "xfce"
    {
        grip_style = none
        smooth_edge = true
        boxfill
        {
            fill_style = gradient
            orientation = horizontal
            shade_start = 1.00
            shade_end = 0.97
        }
    }
}

style "scale" = "default"
{
    xthickness = 1
    ythickness = 1
    engine "xfce"
    {
        #grip_style = none
        smooth_edge = false
    }
}

style "menu" = "default"
{
    xthickness = 1
    ythickness = 1

    bg[PRELIGHT] = "#bebebe"
}

widget_class "*"                   style "default"

widget_class "*Menu*"              style "menu"

widget_class "*GtkProgress*"       style "progress"
class "*GtkProgress*"              style "progress"

widget_class "*GtkVScrollbar*"     style "scrollbar"
class "*GtkVScrollbar*"            style "scrollbar"
widget_class "*GtkHScrollbar*"     style "scrollbar"
class "*GtkHScrollbar*"            style "scrollbar"

widget_class "*GtkProgress*"       style "progress" 
class "*GtkProgress*"              style "progress" 

widget_class "*GtkHScale*"         style "scale"
class "*GtkHScale*"                style "scale"
widget_class "*GtkVScale*"         style "scale"
class "*GtkVScale*"                style "scale"

widget "xfwm"                      style "titlebar"
widget "xfwm4-tabwin*"             style "titlebar"
class "MetaFrames"                 style "titlebar"
widget_class "MetaFrames"          style "titlebar"
